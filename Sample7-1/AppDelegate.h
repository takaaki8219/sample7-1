//
//  AppDelegate.h
//  Sample7-1
//
//  Created by Kakinuma Takaaki on 2012/11/20.
//  Copyright (c) 2012年 Kakinuma Takaaki. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
